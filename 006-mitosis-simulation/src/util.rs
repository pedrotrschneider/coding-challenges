// util.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use macroquad::prelude::*;

#[allow(dead_code)]
pub fn map_f32(value: f32, in_min: f32, in_max: f32, out_min: f32, out_max: f32) -> f32 {
    return (value - in_min) / (in_max - in_min) * (out_max - out_min) + out_min;
}

#[allow(dead_code)]
pub fn sq_dist(x1: f32, y1: f32, x2: f32, y2: f32) -> f32 {
    return (x2 - x1).powi(2) + (y2 - y1).powi(2);
}

#[allow(dead_code)]
pub fn dist(x1: f32, y1: f32, x2: f32, y2: f32) -> f32 {
    return sq_dist(x1, y1, x2, y2).sqrt();
}
