// main.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use macroquad::prelude::*;

mod util;
#[allow(unused_imports)]
use util::*;

mod cell;
use cell::Cell;

fn window_conf() -> Conf {
    return Conf {
        window_title: "Mitosis Simulation".to_string(),
        window_resizable: false,
        window_width: 1000,
        window_height: 1000,
        sample_count: 2,
        ..Default::default()
    };
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut cells: Vec<Cell> = Vec::new();
    cells.push(Cell::new(
        screen_width() * 0.5f32,
        screen_height() * 0.5f32,
        100f32,
    ));

    loop {
        let dt = get_frame_time();

        // Update stuff
        let mut new_cells: Vec<Cell> = Vec::new();
        for cell in cells.iter_mut() {
            cell.update(dt, &mut new_cells);
        }
        cells.retain(|cell| !cell.dead);
        cells.append(&mut new_cells);

        // Clear the background
        clear_background(Color::new(0.2f32, 0.2f32, 0.2f32, 1f32));

        // Draw some stuff
        for cell in cells.iter() {
            cell.show();
        }

        draw_text(&format!("fps: {}", get_fps()), 20f32, 40f32, 30f32, PURPLE);

        next_frame().await;
    }
}
