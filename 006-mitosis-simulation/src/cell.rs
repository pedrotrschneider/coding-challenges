// cell.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::util;
use macroquad::prelude::*;
use ::rand;

pub struct Cell {
    pos: Vec2,
    r: f32,
    color: Color,
    pub dead: bool,
}

impl Cell {
    const SPEED: f32 = 200f32;

    pub fn new(x: f32, y: f32, r: f32) -> Cell {
        let red = util::map_f32(rand::random::<f32>(), 0f32, 1f32, 0.4f32, 1f32);
        let blue = util::map_f32(rand::random::<f32>(), 0f32, 1f32, 0.4f32, 1f32);
        let color = Color::new(red, 0f32, blue, 0.4f32);
        return Cell {
            pos: Vec2::new(x, y),
            r,
            color,
            dead: false,
        };
    }

    pub fn update(&mut self, dt: f32, new_cells: &mut Vec<Cell>) {
        let vel_x = util::map_f32(rand::random::<f32>(), 0f32, 1f32, -1f32, 1f32);
        let vel_y = util::map_f32(rand::random::<f32>(), 0f32, 1f32, -1f32, 1f32);
        let vel = Vec2::new(vel_x, vel_y).normalize();
        self.pos += vel * Cell::SPEED * dt;

        if is_mouse_button_pressed(MouseButton::Left) {
            let (mx, my) = mouse_position();
            if util::sq_dist(self.pos.x, self.pos.y, mx, my) <= self.r.powi(2) {
                new_cells.push(Cell::new(self.pos.x, self.pos.y, self.r * 0.8f32));
                new_cells.push(Cell::new(self.pos.x, self.pos.y, self.r * 0.8f32));
                self.dead = true;
            }
        }
    }

    pub fn show(&self) {
        draw_circle(self.pos.x, self.pos.y, self.r, self.color);
    }
}
