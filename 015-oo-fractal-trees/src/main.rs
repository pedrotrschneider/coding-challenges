use std::f32::consts::PI;

use macroquad::prelude::*;
use macroquad::ui::{hash, root_ui, widgets};

mod util;
#[allow(unused_imports)]
use util::*;

mod branch;
use branch::Branch;

fn window_conf() -> Conf {
    return Conf {
        window_title: "Object Oriented Fractal Trees".to_string(),
        window_resizable: false,
        window_width: 1000,
        window_height: 1000,
        sample_count: 2,
        ..Default::default()
    };
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut len = 220f32;
    let mut angle = PI * 0.25f32;
    let mut falloff = 0.8f32;

    let mut tree: Vec<Branch> = Vec::new();
    tree.push(Branch::new(
        Vec2::new(screen_width() * 0.5f32, screen_height()),
        Vec2::new(screen_width() * 0.5, screen_height() - len),
    ));

    loop {
        clear_background(Color::new(0.2f32, 0.2f32, 0.2f32, 1f32));

        for branch in tree.iter() {
            branch.show();
        }

        widgets::Window::new(
            hash!(),
            Vec2::new(100f32, 100f32),
            Vec2::new(400f32, 210f32),
        )
        .label("Slider")
        .ui(&mut *root_ui(), |ui| {
            ui.label(None, &format!("fps: {}", get_fps()));
            ui.slider(hash!(), "Length", 0f32..500f32, &mut len);
            ui.slider(hash!(), "Angle", 0f32..(PI), &mut angle);
            ui.slider(hash!(), "Falloff", 0f32..1f32, &mut falloff);

            ui.separator();
            ui.separator();
            ui.separator();
            ui.separator();

            if ui.button(None, "Branch") {
                let mut new_branches: Vec<Branch> = Vec::new();
                for branch in tree.iter_mut() {
                    match branch.branch(angle, falloff) {
                        Some((b1, b2)) => {
                            new_branches.push(b1);
                            new_branches.push(b2);
                        }
                        None => {}
                    }
                }

                tree.append(&mut new_branches);
            }

            ui.separator();
            ui.separator();
            ui.separator();
            ui.separator();

            if ui.button(None, "Clear") {
                tree.clear();
                tree.push(Branch::new(
                    Vec2::new(screen_width() * 0.5f32, screen_height()),
                    Vec2::new(screen_width() * 0.5, screen_height() - len),
                ));
            }

            ui.separator();
            ui.separator();
            ui.separator();
            ui.separator();

            if ui.button(None, "Screenshot") {
                get_screen_data().export_png("screenshot.png");
            }
        });

        next_frame().await;
    }
}
