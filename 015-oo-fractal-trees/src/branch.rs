use macroquad::prelude::*;

use crate::util;

pub struct Branch {
    begin: Vec2,
    end: Vec2,
    finished: bool,
}

impl Branch {
    pub fn new(begin: Vec2, end: Vec2) -> Branch {
        return Branch {
            begin,
            end,
            finished: false,
        };
    }

    pub fn show(&self) {
        draw_line(
            self.begin.x,
            self.begin.y,
            self.end.x,
            self.end.y,
            3f32,
            WHITE,
        );
    }

    pub fn branch(&mut self, angle: f32, falloff: f32) -> Option<(Branch, Branch)> {
        if self.finished {
            return None;
        }

        let dir = (self.end - self.begin) * falloff;
        let dir_right = util::vec2_rotated(&dir, angle);
        let dir_left = util::vec2_rotated(&dir, -angle);

        let b1 = Branch::new(self.end.clone(), self.end + dir_right);
        let b2 = Branch::new(self.end.clone(), self.end + dir_left);

        self.finished = true;

        return Some((b1, b2));
    }
}
