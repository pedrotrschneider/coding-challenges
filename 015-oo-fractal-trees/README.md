# Object Oriented Fractal Trees

[Youtube video](https://www.youtube.com/watch?v=fcdNSZ9IzJM)

Remake in Rust of Daniel Shiffman's Coding Challenge #15 Object Oriented Fractal Trees

![](screenshot.png)

You can run this coding challenge with

```
cargo run --release
```

If you want to turn v-sync off, run it with 

```
vblank_mode=0 cargo run --release
```