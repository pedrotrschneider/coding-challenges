# World.gd
# Copyright (C) 2022  Pedro Tonini Rosenberg Schneider
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https:www.gnu.org/licenses/>.

extends Spatial

onready var mesh_node = $MeshInstance
onready var mesh_mat = mesh_node.get_surface_material(0)

func _ready():
	make_wire();

# Transform the mesh into a wireframe
func make_wire():
	var array = mesh_node.mesh.surface_get_arrays(0)
	mesh_node.mesh = ArrayMesh.new()
	mesh_node.mesh.add_surface_from_arrays(Mesh.PRIMITIVE_LINES, array)
	mesh_node.set_surface_material(0, mesh_mat)
