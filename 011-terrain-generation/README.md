# 3D Terrain Generation

[Youtube video](https://www.youtube.com/watch?v=IKB1hWWedMk)

Remake in Rust of Daniel Shiffman's Coding Challenge #11 3D Terrain Generation

![](screenshot.png)

You can run this coding challenge by importing the `project.godot` file in the Godot Game Engine