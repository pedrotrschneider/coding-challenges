# Maze Generator

[Youtube video part 1](https://www.youtube.com/watch?v=HyK_Q5rrcr4)

[Youtube video part 2](https://www.youtube.com/watch?v=D8UgRyRnvXU)

[Youtube video part 3](https://www.youtube.com/watch?v=8Ju_uxJ9v44)

[Youtube video part 4](https://www.youtube.com/watch?v=_p5IH0L63wo)

Remake in Rust of Daniel Shiffman's Coding Challenge #10 Maze Generator

![](screenshot.png)

You can run this coding challenge with

```
cargo run --release
```

If you want to turn v-sync off, run it with 

```
vblank_mode=0 cargo run --release
```