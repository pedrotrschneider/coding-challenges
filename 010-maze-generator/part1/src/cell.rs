// cell.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use macroquad::prelude::*;

use crate::GRID_SIZE;

pub struct Cell {
    i: u32,
    j: u32,
    walls: (bool, bool, bool, bool),
}

impl Cell {
    pub fn new(i: u32, j: u32) -> Cell {
        let walls = (true, true, true, true);
        return Cell { i, j, walls };
    }

    pub fn show(&self) {
        let x0 = self.i as f32 * GRID_SIZE;
        let x1 = x0 + GRID_SIZE;
        let y0 = self.j as f32 * GRID_SIZE;
        let y1 = y0 + GRID_SIZE;

        // Top wall
        if self.walls.0 {
            draw_line(x0, y0, x1, y0, 2f32, WHITE);
        }
        // Right wall
        if self.walls.1 {
            draw_line(x1, y0, x1, y1, 2f32, WHITE);
        }
        // Bottom wall
        if self.walls.2 {
            draw_line(x0, y1, x1, y1, 2f32, WHITE);
        }
        // Left wall
        if self.walls.3 {
            draw_line(x0, y0, x0, y1, 2f32, WHITE);
        }
    }
}
