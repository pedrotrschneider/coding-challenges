// main.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use macroquad::prelude::*;

mod util;
#[allow(unused_imports)]
use util::*;

mod cell;
use cell::Cell;

const GRID_SIZE: f32 = 100f32;

fn window_conf() -> Conf {
    return Conf {
        window_title: "Maze Generator part 3".to_string(),
        window_resizable: false,
        window_width: 1000,
        window_height: 1000,
        sample_count: 2,
        ..Default::default()
    };
}

#[macroquad::main(window_conf)]
async fn main() {
    let cols: u32 = (screen_width() / GRID_SIZE) as u32;
    let rows: u32 = (screen_height() / GRID_SIZE) as u32;

    let mut cells: Vec<Cell> = Vec::new();
    for j in 0..rows {
        for i in 0..cols {
            cells.push(Cell::new(i as usize, j as usize));
        }
    }

    let mut current = 0;

    let mut frames = 0;
    let target_fps = 5;
    loop {
        // Update stuff
        if frames % (60 / target_fps) == 0 {
            cells[current].visited = true;
            match cells[current].get_random_neighbor(&cells, cols, rows) {
                Some(i) => {
                    remove_walls(current, i, &mut cells);
                    current = i;
                }
                _ => {}
            }
        }

        // Clear the background
        clear_background(BLACK);

        // Draw some stuff
        for cell in cells.iter() {
            cell.show();
        }
        cells[current].highlight();

        draw_text(&format!("fps: {}", get_fps()), 20f32, 40f32, 30f32, PURPLE);

        next_frame().await;

        frames += 1;
    }
}

fn remove_walls(a: usize, b: usize, cells: &mut Vec<Cell>) {
    let ai = cells[a].i;
    let aj = cells[a].j;
    let bi = cells[b].i;
    let bj = cells[b].j;

    let x = ai as i32 - bi as i32;
    if x == 1 {
        cells[a].walls.3 = false;
        cells[b].walls.1 = false;
    } else if x == -1 {
        cells[a].walls.1 = false;
        cells[b].walls.3 = false;
    }

    let y = aj as i32 - bj as i32;
    if y == 1 {
        cells[a].walls.0 = false;
        cells[b].walls.2 = false;
    } else if y == -1 {
        cells[a].walls.2 = false;
        cells[b].walls.0 = false;
    }
}
