// cell.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use macroquad::prelude::*;

use crate::{util, GRID_SIZE};

pub struct Cell {
    pub i: usize,
    pub j: usize,
    pub walls: (bool, bool, bool, bool),
    pub visited: bool,
}

impl Cell {
    pub fn new(i: usize, j: usize) -> Cell {
        let walls = (true, true, true, true);
        let visited = false;

        return Cell {
            i,
            j,
            walls,
            visited,
        };
    }

    pub fn index(i: i32, j: i32, cols: u32, rows: u32) -> Option<usize> {
        if i < 0 || j < 0 || i > cols as i32 - 1 || j > rows as i32 - 1 {
            return None;
        }
        return Some((i + j * cols as i32) as usize);
    }

    pub fn get_random_neighbor(&self, cells: &Vec<Cell>, cols: u32, rows: u32) -> Option<usize> {
        let mut neighbors: [i32; 4] = [-1, -1, -1, -1];
        let mut i = 0;

        // Check top neighbor
        match Cell::index(self.i as i32, self.j as i32 - 1, cols, rows) {
            Some(index) => {
                if !cells[index].visited {
                    neighbors[i] = index as i32;
                    i += 1;
                }
            }
            _ => {}
        }

        // Check right neighbor
        match Cell::index(self.i as i32 + 1, self.j as i32, cols, rows) {
            Some(index) => {
                if !cells[index].visited {
                    neighbors[i] = index as i32;
                    i += 1;
                }
            }
            _ => {}
        }

        // Check bottom neighbor
        match Cell::index(self.i as i32, self.j as i32 + 1, cols, rows) {
            Some(index) => {
                if !cells[index].visited {
                    neighbors[i] = index as i32;
                    i += 1;
                }
            }
            _ => {}
        }

        // Check left neighbor
        match Cell::index(self.i as i32 - 1, self.j as i32, cols, rows) {
            Some(index) => {
                if !cells[index].visited {
                    neighbors[i] = index as i32;
                    i += 1;
                }
            }
            _ => {}
        }

        if i > 0 {
            return Some(neighbors[util::random_f32(0f32, i as f32) as usize] as usize);
        } else {
            return None;
        }
    }

    pub fn show(&self) {
        let x0 = self.i as f32 * GRID_SIZE;
        let x1 = x0 + GRID_SIZE;
        let y0 = self.j as f32 * GRID_SIZE;
        let y1 = y0 + GRID_SIZE;

        if self.visited {
            draw_rectangle(
                x0,
                y0,
                GRID_SIZE,
                GRID_SIZE,
                Color::new(1f32, 0f32, 1f32, 0.4f32),
            );
        }

        // Top wall
        if self.walls.0 {
            draw_line(x0, y0, x1, y0, 2f32, WHITE);
        }
        // Right wall
        if self.walls.1 {
            draw_line(x1, y0, x1, y1, 2f32, WHITE);
        }
        // Bottom wall
        if self.walls.2 {
            draw_line(x0, y1, x1, y1, 2f32, WHITE);
        }
        // Left wall
        if self.walls.3 {
            draw_line(x0, y0, x0, y1, 2f32, WHITE);
        }
    }

    pub fn highlight(&self) {
        let x = self.i as f32 * GRID_SIZE;
        let y = self.j as f32 * GRID_SIZE;

        draw_rectangle(
            x,
            y,
            GRID_SIZE,
            GRID_SIZE,
            Color::new(0f32, 1f32, 0f32, 0.4f32),
        );
    }
}
