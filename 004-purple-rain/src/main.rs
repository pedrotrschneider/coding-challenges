// main.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use ::rand;
use macroquad::prelude::*;

mod util;
#[allow(unused_imports)]
use util::*;

const NUM_DROPS: usize = 500;

struct Drop {
    x: f32,
    y: f32,
    z: f32,
    fall_speed: f32,
    len: f32,
    thickness: f32,
}

impl Drop {
    fn new() -> Drop {
        let x = util::map_f32(rand::random::<f32>(), 0f32, 1f32, 0f32, screen_width());
        let y = util::map_f32(rand::random::<f32>(), 0f32, 1f32, -5000f32, -50f32);
        let z = util::map_f32(rand::random::<f32>(), 0f32, 1f32, 0f32, 20f32);
        let fall_speed = util::map_f32(z, 0f32, 20f32, 4f32, 10f32);
        let len = util::map_f32(z, 0f32, 20f32, 10f32, 20f32);
        let thickness: f32 = util::map_f32(z, 0f32, 20f32, 1f32, 3f32);
        return Drop {
            x,
            y,
            z,
            fall_speed,
            len,
            thickness,
        };
    }

    fn fall(&mut self) {
        self.y += self.fall_speed;
        self.fall_speed += 0.2;

        if self.y > screen_height() {
            self.y = util::map_f32(rand::random::<f32>(), 0f32, 1f32, -100f32, -200f32);
            self.fall_speed = util::map_f32(self.z, 0f32, 20f32, 4f32, 10f32);
        }
    }

    fn show(&self) {
        draw_line(
            self.x,
            self.y,
            self.x,
            self.y + self.len,
            self.thickness,
            Color::new(138f32 / 255f32, 43f32 / 255f32, 226f32 / 255f32, 1f32),
        );
    }
}

fn window_conf() -> Conf {
    return Conf {
        window_title: "Purple Rain".to_string(),
        window_resizable: false,
        window_width: 1280,
        window_height: 720,
        sample_count: 2,
        ..Default::default()
    };
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut drops: Vec<Drop> = Vec::new();
    for _ in 0..NUM_DROPS {
        drops.push(Drop::new());
    }
    loop {
        // Update stuff
        for drop in drops.iter_mut() {
            drop.fall();
        }

        // Clear the background
        clear_background(Color {
            r: 230f32 / 255f32,
            g: 230f32 / 255f32,
            b: 250f32 / 255f32,
            a: 1f32,
        });

        // Draw some stuff
        for drop in drops.iter() {
            drop.show();
        }

        draw_text(&format!("fps: {}", get_fps()), 20f32, 40f32, 30f32, PURPLE);

        next_frame().await;
    }
}
