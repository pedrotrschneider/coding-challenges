# Purple Rain

[Youtube video](https://www.youtube.com/watch?v=KkyIDI6rQJI)

Remake in Rust of Daniel Shiffman's Coding Challenge #4 Purple Rain

![](screenshot.png)

You can run this coding challenge with

```
cargo run --release
```

If you want to turn v-sync off, run it with 

```
vblank_mode=0 cargo run --release
```