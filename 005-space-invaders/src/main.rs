// main.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use macroquad::prelude::*;

mod util;
#[allow(unused_imports)]
use util::*;

mod ship;
use ship::Ship;

mod bullet;

mod invader;
use invader::Invader;

const NUM_COLS: usize = 15;
const NUM_ROWS: usize = 5;

fn window_conf() -> Conf {
    return Conf {
        window_title: "Space Invaders".to_string(),
        window_resizable: false,
        window_width: 1200,
        window_height: 800,
        sample_count: 2,
        ..Default::default()
    };
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut ship = Ship::new();

    // Building the invader formation:
    let mut invaders: Vec<Invader> = Vec::new();

    // Formation horizontal padding and separation
    let x_padd: f32 = 100f32;
    let x_sep: f32 = (screen_width() - (2f32 * x_padd + Invader::SIZE)) / ((NUM_COLS - 1) as f32);

    // Formation vertical height, padding and separation
    let height: f32 = 400f32;
    let y_padd: f32 = 50f32;
    let y_sep: f32 = (height - Invader::SIZE) / ((NUM_ROWS - 1) as f32);

    // Adding the invaders
    for i in 0..NUM_COLS {
        for j in 0..NUM_ROWS {
            let x = x_padd + x_sep * (i as f32);
            let y = y_padd + y_sep * (j as f32);
            invaders.push(Invader::new(x, y));
        }
    }

    loop {
        let dt = get_frame_time();

        // Update stuff
        ship.update(dt);

        let mut should_turn = false;
        for invader in invaders.iter_mut() {
            invader.update(dt, &mut ship.bullets);
            should_turn = should_turn || invader.shoudl_turn(50f32);
        }

        if should_turn {
            for invader in invaders.iter_mut() {
                invader.turn(dt);
            }
        }

        invaders.retain(|invader| invader.alive);

        // Clear the background
        clear_background(BLACK);

        // Draw some stuff
        ship.show();
        for invader in invaders.iter() {
            invader.show();
        }

        draw_text(&format!("fps: {}", get_fps()), 20f32, 40f32, 30f32, PURPLE);

        next_frame().await;
    }
}
