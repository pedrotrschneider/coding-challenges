// bullet.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use macroquad::prelude::*;

pub struct Bullet {
    pub x: f32,
    pub y: f32,
    pub hit: bool,
}

impl Bullet {
    const SPEED: f32 = 500f32;
    pub const SIZE: f32 = 5f32;

    pub fn new(x: f32, y: f32) -> Bullet {
        return Bullet { x, y, hit: false };
    }

    pub fn update(&mut self, dt: f32) {
        self.y -= dt * Bullet::SPEED;
    }

    pub fn show(&self) {
        draw_circle(self.x, self.y, Bullet::SIZE, GREEN);
    }
}
