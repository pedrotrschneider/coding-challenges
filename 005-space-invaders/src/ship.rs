// ship.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::bullet::Bullet;
use macroquad::prelude::*;

pub struct Ship {
    x: f32,
    y: f32,
    pub bullets: Vec<Bullet>,
    cooldown: f32,
}

impl Ship {
    const SHIP_SPEED: f32 = 250f32;
    const SHOT_COOLDOWN: f32 = 0.5f32;

    pub fn new() -> Ship {
        let x = screen_width() / 2f32;
        return Ship {
            x,
            y: screen_height() - 50f32,
            bullets: Vec::new(),
            cooldown: 0f32,
        };
    }

    pub fn update(&mut self, dt: f32) {
        let x_move = match (is_key_down(KeyCode::Left), is_key_down(KeyCode::Right)) {
            (true, false) => -1f32,
            (false, true) => 1f32,
            _ => 0f32,
        };
        self.x += x_move * dt * Ship::SHIP_SPEED;

        self.x = 70f32.max((screen_width() - 70f32).min(self.x));

        if self.cooldown <= 0f32 && is_key_down(KeyCode::Space) {
            let new_bullet = Bullet::new(self.x, self.y - 20f32);
            self.bullets.push(new_bullet);
            self.cooldown = Ship::SHOT_COOLDOWN;
        }

        for bullet in self.bullets.iter_mut() {
            bullet.update(dt);
        }
        self.bullets.retain(|bullet| !bullet.hit);

        self.cooldown -= dt;
    }

    pub fn show(&self) {
        let v1: Vec2 = Vec2::new(self.x, self.y - 20f32);
        let v2: Vec2 = Vec2::new(self.x + 20f32, self.y + 20f32);
        let v3: Vec2 = Vec2::new(self.x - 20f32, self.y + 20f32);
        draw_triangle(v1, v2, v3, WHITE);

        for bullet in self.bullets.iter() {
            bullet.show();
        }
    }
}
