// invader.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::bullet::Bullet;
use crate::util;
use macroquad::prelude::*;

pub struct Invader {
    x: f32,
    y: f32,
    dir: f32,
    pub alive: bool,
}

impl Invader {
    pub const SIZE: f32 = 40f32;
    const SPEED: f32 = 50f32;
    const DESCEND_SPEED: f32 = 200f32;

    pub fn new(x: f32, y: f32) -> Invader {
        return Invader {
            x,
            y,
            dir: -1f32,
            alive: true,
        };
    }

    pub fn update(&mut self, dt: f32, bullets: &mut Vec<Bullet>) {
        self.x += dt * Invader::SPEED * self.dir;

        for bullet in bullets.iter_mut() {
            let cx = self.x + Invader::SIZE * 0.5f32;
            let cy = self.y + Invader::SIZE * 0.5f32;
            if util::sq_dist(cx, cy, bullet.x, bullet.y)
                < (Invader::SIZE * 0.5f32 + Bullet::SIZE).powi(2)
            {
                self.alive = false;
                bullet.hit = true;
            }
        }
    }

    pub fn turn(&mut self, dt: f32) {
        self.dir *= -1f32;
        self.y += Invader::DESCEND_SPEED * dt;
    }

    pub fn shoudl_turn(&self, x_padd: f32) -> bool {
        return (self.dir < 0f32 && self.x < x_padd)
            || (self.dir > 0f32 && self.x > screen_width() - x_padd - Invader::SIZE);
    }

    pub fn show(&self) {
        draw_rectangle(self.x, self.y, Invader::SIZE, Invader::SIZE, RED);
    }
}
