# Space Invaders

[Youtube video](https://www.youtube.com/watch?v=biN3v3ef-Y0)

Remake in Rust of Daniel Shiffman's Coding Challenge #5 Space Invaders

![](screenshot.png)

You can run this coding challenge with

```
cargo run --release
```

If you want to turn v-sync off, run it with 

```
vblank_mode=0 cargo run --release
```