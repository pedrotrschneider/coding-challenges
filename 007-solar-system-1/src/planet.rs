// planetmain.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::util;
use macroquad::prelude::*;
use std::f32::consts::PI;

pub struct Planet {
    radius: f32,
    angle: f32,
    distance: f32,
    orbit_speed: f32,
    planets: Vec<Planet>,
}

impl Planet {
    const MAX_LEVEL: u32 = 3;

    pub fn new(radius: f32, distance: f32) -> Planet {
        return Planet {
            radius,
            angle: util::random_f32(0f32, PI),
            distance,
            orbit_speed: util::random_f32(-1f32, 1f32),
            planets: Vec::new(),
        };
    }

    pub fn spawn_moons(&mut self, total: usize, level: u32) {
        if level >= Planet::MAX_LEVEL {
            return;
        }
        for _ in 0..total {
            let radius = self.radius / (level + 1) as f32;
            let distance = util::random_f32(self.radius + radius, (self.radius + radius) * 3f32);
            let mut new_planet = Planet::new(radius, distance);
            new_planet.spawn_moons(total / 2, level + 1);
            self.planets.push(new_planet);
        }
    }

    pub fn update(&mut self, dt: f32) {
        self.angle += self.orbit_speed * dt;

        for planet in self.planets.iter_mut() {
            planet.update(dt);
        }
    }

    pub fn show(&self, gl: &mut QuadGl) {
        gl.push_model_matrix(glam::Mat4::from_rotation_z(self.angle));
        gl.push_model_matrix(glam::Mat4::from_translation(Vec3::new(
            self.distance,
            0f32,
            0f32,
        )));

        draw_circle(
            0f32,
            0f32,
            self.radius,
            Color::new(1f32, 1f32, 1f32, 0.5f32),
        );

        for planet in self.planets.iter() {
            planet.show(gl);
        }

        gl.pop_model_matrix();
        gl.pop_model_matrix();
    }
}
