// main.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use macroquad::prelude::*;

mod util;
#[allow(unused_imports)]
use util::*;

mod planet;
use planet::Planet;

fn window_conf() -> Conf {
    return Conf {
        window_title: "Solar System 1".to_string(),
        window_resizable: false,
        window_width: 1000,
        window_height: 1000,
        sample_count: 2,
        ..Default::default()
    };
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut sun = Planet::new(200f32, 0f32);
    sun.spawn_moons(5, 1);

    let gl = unsafe { get_internal_gl().quad_gl };

    loop {
        let dt = get_frame_time();

        // Update stuff
        sun.update(dt);

        // Clear the background
        clear_background(BLACK);

        // Draw some stuff
        gl.push_model_matrix(glam::Mat4::from_translation(Vec3::new(
            screen_width() * 0.5f32,
            screen_height() * 0.5f32,
            0f32,
        )));
        gl.push_model_matrix(glam::Mat4::from_scale(Vec3::new(
            0.5f32,
            0.5f32,
            1f32
        )));
        sun.show(gl);
        gl.pop_model_matrix();
        gl.pop_model_matrix();

        // draw_text(&format!("fps: {}", get_fps()), 20f32, 40f32, 30f32, PURPLE);

        next_frame().await;
    }
}
