# Solar System Part 1

[Youtube video](https://www.youtube.com/watch?v=l8SiJ-RmeHU)

Remake in Rust of Daniel Shiffman's Coding Challenge #7 Solar System Part 1

![](screenshot.png)

You can run this coding challenge with

```
cargo run --release
```

If you want to turn v-sync off, run it with 

```
vblank_mode=0 cargo run --release
```