# Starfield

[Youtube video](https://www.youtube.com/watch?v=17WoOqgXsRM&t=502s)

Remake in Rust of Daniel Shiffman's Coding Challenge #1 Starfield

![](screenshot.png)

You can run this coding challenge with

```
cargo run --release
```

If you want to turn v-sync off, run it with 

```
vblank_mode=0 cargo run --release
```