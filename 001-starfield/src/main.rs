// main.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use macroquad::prelude::*;

mod star;
mod util;

use star::Star;

pub const NUM_STARS: usize = 400;

fn window_conf() -> Conf {
    return Conf {
        window_title: "Starfield".to_string(),
        window_resizable: false,
        window_width: 1080,
        window_height: 1080,
        sample_count: 2,
        ..Default::default()
    };
}

#[macroquad::main(window_conf)]
async fn main() {
    let camera = Camera2D {
        zoom: Vec2::new(2f32 / screen_width(), 2f32 / screen_height()),
        ..Default::default()
    };

    let mut stars: Vec<Star> = Vec::new();
    for _ in 0..NUM_STARS {
        stars.push(Star::new());
    }

    loop {
        for star in stars.iter_mut() {
            star.update(get_frame_time());
        }

        clear_background(BLACK);

        set_camera(&camera);

        for star in stars.iter_mut() {
            star.draw();
        }

        set_default_camera();

        draw_text(&format!("fps: {}", get_fps()), 20f32, 40f32, 30f32, PURPLE);

        next_frame().await;
    }
}
