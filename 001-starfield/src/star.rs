// star.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::util;
use macroquad::prelude::*;

const STAR_SPEED: f32 = 2000.0;

#[derive(Copy, Clone)]
pub struct Star {
    x: f32,
    y: f32,
    z: f32,
    sx: f32,
    sy: f32,
}

impl Star {
    pub fn new() -> Star {
        let x = util::random_f32(-screen_width() * 0.5, screen_width() * 0.5);
        let y = util::random_f32(-screen_height() * 0.5, screen_height() * 0.5);
        let z = util::random_f32(0f32, screen_width());
        return Star {
            x,
            y,
            z,
            sx: 0f32,
            sy: 0f32,
        };
    }

    pub fn update(&mut self, dt: f32) {
        self.z -= STAR_SPEED * dt;
        self.sx = util::map_range(
            self.x / self.z,
            -1f32,
            1f32,
            -screen_width() * 0.5,
            screen_width() * 0.5,
        );
        self.sy = util::map_range(
            self.y / self.z,
            -1f32,
            1f32,
            -screen_height() * 0.5,
            screen_height() * 0.5,
        );

        // Check if the star is offscreen and prevent it from beeing stuck in the middle
        if self.sx.abs() >= screen_width() * 0.5
            || self.sy.abs() >= screen_height() * 0.5
            || (self.sx.abs() < 10f32 && self.sy.abs() < 10f32)
        {
            self.reset();
        }
    }

    pub fn draw(&self) {
        let r = util::map_range(self.z, 0f32, screen_width(), 16f32, 0f32);
        draw_circle(self.sx, self.sy, r, WHITE);
    }

    fn reset(&mut self) {
        self.z = screen_width();
        self.x = util::random_f32(-screen_width() * 0.5, screen_width() * 0.5);
        self.y = util::random_f32(-screen_height() * 0.5, screen_height() * 0.5);
    }
}
