# Planet.gd
# Copyright (C) 2022  Pedro Tonini Rosenberg Schneider
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https:www.gnu.org/licenses/>.

class_name Planet
extends Spatial

const MAX_LEVEL: int = 3;

export(NodePath) onready var mesh_instance = self.get_node(mesh_instance) as MeshInstance;
export(NodePath) onready var visual = self.get_node(visual) as Spatial;
export(NodePath) onready var planets = self.get_node(planets) as Spatial;

export var planet_scene: PackedScene = load("res://planet/Planet.tscn");
export var radius: float = 0.5;
export var distance: float = 0.0;
export var orbit_speed: float = 0.0;

var angle: float = 0.0;


func _ready() -> void:
	var sphere_mesh: SphereMesh = SphereMesh.new();
	sphere_mesh.radius = radius;
	sphere_mesh.height = 2 * radius;
	mesh_instance.mesh = sphere_mesh;
	
	var orbit: Vector3 = Vector3(0.0, 0.0, 1.0);
	orbit *= distance;
	visual.transform.origin = orbit;


func _process(delta: float) -> void:
	if abs(orbit_speed) > 0.0:
		self.rotate_y(orbit_speed * delta);


func spawn_planets(amount: int, level: int) -> void:
	if level >= MAX_LEVEL:
		return;
	for i in amount:
		var new_radius: float = radius * 0.5;
		var new_distance: float = rand_range((radius + new_radius) * 3.0, (radius + new_radius) * 8.0) / (level * level);
		
		var new_planet = planet_scene.instance();
		new_planet.radius = new_radius;
		new_planet.distance = new_distance;
		if orbit_speed == 0.0:
			new_planet.orbit_speed = 2.0;
		else:
			new_planet.orbit_speed = orbit_speed / level;
		planets.add_child(new_planet);
		new_planet.spawn_planets(amount / 2, level + 1);
