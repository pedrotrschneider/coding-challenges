extends Spatial

export(NodePath) onready var outer_cylinder = self.get_node(outer_cylinder) as CSGCylinder;
export(NodePath) onready var inner_cylinder = self.get_node(inner_cylinder) as CSGCylinder;

export var radius: float;

func _ready() -> void:
	outer_cylinder.radius = radius;
	inner_cylinder.radius = radius - 0.01;
