# Solar System Part 3

[Youtube video](https://www.youtube.com/watch?v=FGAwi7wpU8c)

Remake in Rust of Daniel Shiffman's Coding Challenge #9 Solar System Part 3

![](screenshot.png)

You can run this coding challenge by importing the `project.godot` file in the Godot Game Engine