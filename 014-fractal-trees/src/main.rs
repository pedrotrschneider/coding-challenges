use std::f32::consts::PI;

use macroquad::prelude::*;
use macroquad::ui::{hash, root_ui, widgets};

mod util;
#[allow(unused_imports)]
use util::*;

const RECURSION_LIMIT: u8 = 11;

fn window_conf() -> Conf {
    return Conf {
        window_title: "Fractal Trees".to_string(),
        window_resizable: false,
        window_width: 1000,
        window_height: 1000,
        sample_count: 2,
        ..Default::default()
    };
}

#[macroquad::main(window_conf)]
async fn main() {
    let len = 250f32;
    let mut angle = 0f32;
    let mut animate = false;
    let mut dir = 1f32;
    let mut anim_speed: f32 = 0.5f32;

    let gl = unsafe { get_internal_gl().quad_gl };

    loop {
        let dt = get_frame_time();
        if animate {
            angle += dir * anim_speed * dt;

            if angle >= PI || angle <= 0f32 {
                dir *= -1f32;
            }
        }

        clear_background(Color::new(0.2f32, 0.2f32, 0.2f32, 1f32));

        util::translate(gl, screen_width() * 0.5f32, screen_height(), 0f32);
        branch(gl, len, 0, angle);
        gl.pop_model_matrix();

        widgets::Window::new(
            hash!(),
            Vec2::new(100f32, 100f32),
            Vec2::new(400f32, 160f32),
        )
        .label("Slider")
        .ui(&mut *root_ui(), |ui| {
            ui.label(None, &format!("fps: {}", get_fps()));
            ui.slider(hash!(), "Angle", 0f32..(PI), &mut angle);

            ui.separator();
            ui.separator();
            ui.separator();
            ui.separator();

            ui.checkbox(hash!(), "Animate", &mut animate);
            ui.slider(hash!(), "Anim Speed", 0f32..2f32, &mut anim_speed);

            ui.separator();
            ui.separator();
            ui.separator();
            ui.separator();

            if ui.button(None, "Screenshot") {
                get_screen_data().export_png("screenshot.png");
            }
        });

        next_frame().await;
    }
}

fn branch(gl: &mut QuadGl, len: f32, level: u8, angle: f32) {
    draw_line(0f32, 0f32, 0f32, -len, 2f32, WHITE);
    util::translate(gl, 0f32, -len, 0f32);
    if level < RECURSION_LIMIT {
        // Left branch
        util::rotate_2d(gl, angle);
        branch(gl, len * 0.8, level + 1, angle);
        gl.pop_model_matrix();

        // Right branch
        util::rotate_2d(gl, -angle);
        branch(gl, len * 0.8, level + 1, angle);
        gl.pop_model_matrix();
    }
    gl.pop_model_matrix();
}
