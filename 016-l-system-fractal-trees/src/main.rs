use std::f32::consts::PI;

use macroquad::prelude::*;
use macroquad::ui::{hash, root_ui, widgets};

mod util;
#[allow(unused_imports)]
use util::*;

struct Rule {
    read: char,
    sub: String,
}

fn window_conf() -> Conf {
    return Conf {
        window_title: "L-system Fractal Trees".to_string(),
        window_resizable: false,
        window_width: 1600,
        window_height: 1600,
        sample_count: 2,
        ..Default::default()
    };
}

#[macroquad::main(window_conf)]
async fn main() {
    let len = 100f32;
    let angle = PI / 6f32;

    let axiom = "F";
    let mut sentence = axiom.to_string();

    let mut rules: Vec<Rule> = Vec::new();
    rules.push(Rule {
        read: 'F',
        sub: "FF+[+F-F-F]-[-F+F+F]".to_string(),
    });

    let mut lines: Vec<(Vec2, Vec2)> = Vec::new();

    loop {
        widgets::Window::new(
            hash!(),
            Vec2::new(100f32, 100f32),
            Vec2::new(400f32, 210f32),
        )
        .label("Slider")
        .ui(&mut *root_ui(), |ui| {
            ui.label(None, &format!("fps: {}", get_fps()));

            ui.separator();
            ui.separator();
            ui.separator();
            ui.separator();

            if ui.button(None, "Generate") {
                let mut new_sentence = String::new();
                for c in sentence.chars() {
                    let mut found = false;
                    for rule in rules.iter() {
                        if c == rule.read {
                            new_sentence.push_str(&rule.sub);
                            found = true;
                            break;
                        }
                    }
                    if !found {
                        new_sentence.push(c);
                    }
                }
                sentence = new_sentence;

                clear_background(BLACK);

                let current = Vec2::new(screen_width() * 0.5f32, screen_height());
                for c in sentence.chars() {
                    if c == 'F' {
                        let mut next = current;
                        
                        draw_line(0f32, 0f32, 0f32, -len, 2f32, WHITE);
                    } else if c == '+' {
                    } else if c == '-' {
                    } else if c == '[' {
                    } else if c == ']' {
                    }
                }
            }
        });

        next_frame().await;
    }
}
