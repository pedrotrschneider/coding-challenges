# Lorenz Attractor

[Youtube video](https://www.youtube.com/watch?v=f0lkz2gSsIk)

Remake using Godot of Daniel Shiffman's Coding Challenge #1 Starfield

![](screenshot.png)

You can run this coding challenge by importing the `project.godot` file in the Godot Game Engine