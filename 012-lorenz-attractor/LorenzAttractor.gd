extends Spatial

onready var ig: ImmediateGeometry = $InstantGeometry;

export(Color) var color;
export var priority: int = 0;

export var x: float = 0.01;
export var y: float = 0.0;
export var z: float = 0.0;

var a: float = 10.0;
var b: float = 28.0;
var c: float = 8.0 / 3.0;

var points: PoolVector3Array = [];


func _ready() -> void:
	var new_material: SpatialMaterial = SpatialMaterial.new();
	new_material.render_priority = priority;
	new_material.emission_enabled = true;
	new_material.emission = color;
	new_material.albedo_color = color;
	new_material.flags_transparent = true;
	ig.material_override = new_material;


func _physics_process(delta: float) -> void:
	var dx: float = (a * (y - x)) * delta;
	var dy: float = (x * (b - z) - y) * delta;
	var dz: float = (x * y - c * z) * delta;
	x += dx;
	y += dy;
	z += dz;
	
	var current: Vector3 = Vector3(x, y, z) * 0.05;
	points.append(current);
	
	ig.clear();
	ig.begin(Mesh.PRIMITIVE_LINE_STRIP);
	for p in points:
		ig.add_vertex(p);
	ig.end();
