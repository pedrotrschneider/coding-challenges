# Reaction Diffusion

[Youtube video](https://www.youtube.com/watch?v=BV9ny785UNc)

Remake in Rust of Daniel Shiffman's Coding Challenge #13 Reaction Diffusion

![](capture.gif)

You can run this coding challenge with

```
cargo run --release
```

If you want to turn v-sync off, run it with 

```
vblank_mode=0 cargo run --release
```