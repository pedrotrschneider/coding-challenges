use macroquad::prelude::*;

mod util;
#[allow(unused_imports)]
use util::*;

const TEXTURE_WIDTH: usize = 400;
const TEXTURE_HEIGHT: usize = 400;
const BATCH_ITERATIONS: usize = 5;

const DA: f32 = 1f32;
const DB: f32 = 0.5f32;
const FEED: f32 = 0.055f32;
const KILL: f32 = 0.062f32;

const WEIGHTS: [[f32; 3]; 3] = [
    [0.05f32, 0.2f32, 0.05f32],
    [0.2f32, -1f32, 0.2f32],
    [0.05f32, 0.2f32, 0.05f32],
];

enum State {
    Drawing,
    Simulating,
}

fn window_conf() -> Conf {
    return Conf {
        window_title: "Reaction Diffusion Algorithm".to_string(),
        window_resizable: false,
        window_width: 1000,
        window_height: 1000,
        sample_count: 2,
        ..Default::default()
    };
}

#[macroquad::main(window_conf)]
async fn main() {
    let screen_texture_ratio = TEXTURE_WIDTH as f32 / screen_width();
    let mut simulation_state = State::Drawing;

    // Creating the initial grid
    let mut grid: Vec<Vec<(f32, f32)>> = Vec::new();
    let mut next: Vec<Vec<(f32, f32)>> = Vec::new();
    for x in 0..TEXTURE_WIDTH {
        grid.push(Vec::new());
        next.push(Vec::new());
        for _ in 0..TEXTURE_HEIGHT {
            grid[x].push((1f32, 0f32));
            next[x].push((1f32, 0f32));
        }
    }

    let mut image = Image::gen_image_color(
        TEXTURE_HEIGHT as u16,
        TEXTURE_HEIGHT as u16,
        Color::new(1f32, 0f32, 0f32, 1f32),
    );
    let texture = Texture2D::from_image(&image);

    let mut pen_radius = 10f32;
    let mouse_wheel_speed = 5f32;
    let mut draw_interface = true;

    loop {
        let dt = 0.8;

        // Draw some stuff
        for x in 0..TEXTURE_WIDTH {
            for y in 0..TEXTURE_HEIGHT {
                let c = next[x][y].0 - next[x][y].1;
                let col = Color::new(c, c, c, 1f32);
                image.set_pixel(x as u32, y as u32, col);
            }
        }

        texture.update(&image);
        draw_texture(texture, 0f32, 0f32, WHITE);
        draw_texture_ex(
            texture,
            0f32,
            0f32,
            WHITE,
            DrawTextureParams {
                dest_size: Some(Vec2::new(screen_width(), screen_height())),
                ..Default::default()
            },
        );

        match simulation_state {
            State::Drawing => {
                if is_key_pressed(KeyCode::Space) {
                    simulation_state = State::Simulating;
                }

                if is_mouse_button_pressed(MouseButton::Left) {
                } else if is_mouse_button_released(MouseButton::Left) {
                }

                let (mx, my) = mouse_position();
                let (_, mw) = mouse_wheel();
                pen_radius += mw * mouse_wheel_speed;

                if is_mouse_button_down(MouseButton::Left) {
                    let x1 = ((mx - pen_radius) * screen_texture_ratio) as usize;
                    let x2 = ((mx + pen_radius) * screen_texture_ratio) as usize;
                    let y1 = ((my - pen_radius) * screen_texture_ratio) as usize;
                    let y2 = ((my + pen_radius) * screen_texture_ratio) as usize;

                    if x1 >= 1 && x2 <= TEXTURE_WIDTH - 1 {
                        if y1 >= 1 && y2 <= TEXTURE_HEIGHT - 1 {
                            for i in x1..x2 {
                                for j in y1..y2 {
                                    grid[i][j].1 = 1f32;
                                    next[i][j].1 = 1f32;
                                }
                            }
                        }
                    }
                }

                draw_rectangle_lines(
                    mx - pen_radius,
                    my - pen_radius,
                    pen_radius * 2f32,
                    pen_radius * 2f32,
                    4f32,
                    GREEN,
                );
            }
            State::Simulating => {
                if is_key_pressed(KeyCode::Space) {
                    simulation_state = State::Drawing;
                }

                // Update stuff
                for _ in 0..BATCH_ITERATIONS {
                    for x in 1..(TEXTURE_WIDTH - 1) {
                        for y in 1..(TEXTURE_HEIGHT - 1) {
                            let a = grid[x][y].0;
                            let b = grid[x][y].1;

                            let (laplace_a, laplace_b) = laplace(x, y, &grid);

                            next[x][y].0 =
                                a + ((DA * laplace_a) - (a * b * b) + (FEED * (1f32 - a))) * dt;
                            next[x][y].1 =
                                b + ((DB * laplace_b) + (a * b * b) - ((KILL + FEED) * b)) * dt;

                            grid[x][y] = next[x][y];
                        }
                    }
                }
            }
        }

        if is_key_pressed(KeyCode::P) {
            get_screen_data().export_png("screenshot.png");
        }
        if is_key_pressed(KeyCode::I) {
            draw_interface = !draw_interface;
        }

        if draw_interface {
            draw_rectangle_lines(0f32, 0f32, 600f32, 180f32, 4f32, WHITE);
            draw_rectangle(
                0f32,
                0f32,
                600f32,
                180f32,
                Color::new(0f32, 0f32, 0f32, 0.7f32),
            );
            draw_text(&format!("fps: {}", get_fps()), 20f32, 40f32, 30f32, WHITE);
            draw_text(
                &"Pres [space] to pause/continue",
                20f32,
                80f32,
                30f32,
                WHITE,
            );
            draw_text(
                &"Use [mouse wheel] to change the brush size.",
                20f32,
                105f32,
                30f32,
                WHITE,
            );
            draw_text(
                &"Pres [p] to take a screenshot",
                20f32,
                130f32,
                30f32,
                WHITE,
            );
            draw_text(
                &"Pres [i] to toggle this interface",
                20f32,
                155f32,
                30f32,
                WHITE,
            );
        }

        next_frame().await;
    }
}

fn laplace(x: usize, y: usize, grid: &Vec<Vec<(f32, f32)>>) -> (f32, f32) {
    let mut sum = (0f32, 0f32);

    for i in [-1, 0, 1] {
        let xx = (x as i32 + i) as usize;
        let ii = (i + 1) as usize;
        for j in [-1, 0, 1] {
            let yy = (y as i32 + j) as usize;
            let jj = (j + 1) as usize;
            sum.0 += grid[xx][yy].0 * WEIGHTS[ii][jj];
            sum.1 += grid[xx][yy].1 * WEIGHTS[ii][jj];
        }
    }

    return sum;
}
