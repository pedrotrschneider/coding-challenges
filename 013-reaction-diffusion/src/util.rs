use ::rand;
use macroquad::prelude::*;

#[allow(dead_code)]
pub fn map_f32(value: f32, in_min: f32, in_max: f32, out_min: f32, out_max: f32) -> f32 {
    return (value - in_min) / (in_max - in_min) * (out_max - out_min) + out_min;
}

#[allow(dead_code)]
pub fn sq_dist(x1: f32, y1: f32, x2: f32, y2: f32) -> f32 {
    return (x2 - x1).powi(2) + (y2 - y1).powi(2);
}

#[allow(dead_code)]
pub fn dist(x1: f32, y1: f32, x2: f32, y2: f32) -> f32 {
    return sq_dist(x1, y1, x2, y2).sqrt();
}

#[allow(dead_code)]
pub fn random_f32(min: f32, max: f32) -> f32 {
    return map_f32(rand::random::<f32>(), 0f32, 1f32, min, max);
}
