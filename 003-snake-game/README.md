# Snake Game

[Youtube video](https://www.youtube.com/watch?v=AaGK-fj-BAM)

Remake in Rust of Daniel Shiffman's Coding Challenge #2 Snake Game

![](screenshot.png)

You can run this coding challenge with

```
cargo run --release
```

If you want to turn v-sync off, run it with 

```
vblank_mode=0 cargo run --release
```