// snake.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::food::Food;
use crate::util;
use crate::GIRD_SIZE;
use macroquad::prelude::*;
use std::collections::VecDeque;

pub struct Snake {
    x: f32,
    y: f32,
    x_speed: f32,
    y_speed: f32,
    tail: VecDeque<Vec2>,
}

impl Snake {
    pub fn new() -> Snake {
        return Snake {
            x: 0f32,
            y: 0f32,
            x_speed: 1f32,
            y_speed: 0f32,
            tail: VecDeque::new(),
        };
    }

    pub fn update_speed(&mut self) {
        if self.y_speed != 1f32 && is_key_pressed(KeyCode::Up) {
            self.x_speed = 0f32;
            self.y_speed = -1f32;
        } else if self.y_speed != -1f32 && is_key_pressed(KeyCode::Down) {
            self.x_speed = 0f32;
            self.y_speed = 1f32;
        } else if self.x_speed != 1f32 && is_key_pressed(KeyCode::Left) {
            self.x_speed = -1f32;
            self.y_speed = 0f32;
        } else if self.x_speed != -1f32 && is_key_pressed(KeyCode::Right) {
            self.y_speed = 0f32;
            self.x_speed = 1f32;
        }
    }

    pub fn update_pos(&mut self) {
        for mut i in 0..self.tail.len() {
            i = self.tail.len() - 1 - i;
            if i != 0 {
                self.tail[i] = self.tail[i - 1].clone();
            } else {
                self.tail[i] = Vec2::new(self.x, self.y);
            }
        }

        self.x += self.x_speed * GIRD_SIZE;
        self.y += self.y_speed * GIRD_SIZE;

        self.x = 0f32.max((screen_width() - GIRD_SIZE).min(self.x));
        self.y = 0f32.max((screen_width() - GIRD_SIZE).min(self.y));
    }

    pub fn eat(&self, food: &Food) -> bool {
        return util::sq_dist(self.x, self.y, food.x, food.y) < 4f32;
    }

    pub fn grow(&mut self) {
        if self.tail.len() > 0 {
            self.tail.push_back(self.tail.back().unwrap().clone());
        } else {
            self.tail.push_back(Vec2::new(self.x, self.y));
        }
    }

    pub fn died(&self) -> bool {
        for pos in self.tail.iter() {
            if util::sq_dist(self.x, self.y, pos.x, pos.y) < 4f32 {
                return true;
            }
        }
        return false;
    }

    pub fn show(&self) {
        for pos in self.tail.iter() {
            draw_rectangle(pos.x, pos.y, GIRD_SIZE, GIRD_SIZE, WHITE);
            draw_rectangle_lines(pos.x, pos.y, GIRD_SIZE, GIRD_SIZE, 2f32, BLACK);
        }

        draw_rectangle(self.x, self.y, GIRD_SIZE, GIRD_SIZE, WHITE);
        draw_rectangle_lines(self.x, self.y, GIRD_SIZE, GIRD_SIZE, 2f32, BLACK);
    }
}
