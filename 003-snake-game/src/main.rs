// main.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use macroquad::prelude::*;

mod util;

mod snake;
use snake::Snake;

mod food;
use food::Food;

const GIRD_SIZE: f32 = 40f32;

fn window_conf() -> Conf {
    return Conf {
        window_title: "Snake Game".to_string(),
        window_resizable: false,
        window_width: 1000,
        window_height: 1000,
        sample_count: 2,
        ..Default::default()
    };
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut total_frames: u32 = 0;

    let mut snake: Snake = Snake::new();
    let mut food: Food = Food::new();

    loop {
        // Update stuff
        snake.update_speed();

        if total_frames % 5 == 0 {
            snake.update_pos();
            if snake.eat(&food) {
                food = Food::new();
                snake.grow();
            } else if snake.died() {
                snake = Snake::new();
            }
        }

        // Clear the background
        clear_background(BLACK);

        snake.show();
        food.show();

        draw_text(&format!("fps: {}", get_fps()), 20f32, 40f32, 30f32, PURPLE);

        total_frames += 1;
        next_frame().await;
    }
}
