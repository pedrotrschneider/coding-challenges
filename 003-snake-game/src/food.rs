// food.rs
// Copyright (C) 2022  Pedro Tonini Rosenberg Schneider

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::GIRD_SIZE;
use ::rand;
use macroquad::prelude::*;

pub struct Food {
    pub x: f32,
    pub y: f32,
}

impl Food {
    pub fn new() -> Food {
        let x = ((rand::random::<f32>() * screen_width()) as u32) / (GIRD_SIZE as u32)
            * GIRD_SIZE as u32;
        let x = x as f32;

        let y = ((rand::random::<f32>() * screen_height()) as u32) / (GIRD_SIZE as u32)
            * GIRD_SIZE as u32;
        let y = y as f32;

        return Food { x, y };
    }

    pub fn show(&self) {
        draw_rectangle(self.x, self.y, GIRD_SIZE, GIRD_SIZE, RED);
        draw_rectangle_lines(self.x, self.y, GIRD_SIZE, GIRD_SIZE, 2f32, BLACK);
    }
}
