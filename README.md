# Coding Challenges

This is an ongoing project that aims to recreate [Danie Shiffman's Coding Challenges](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6ZiZxtDDRCi6uhfTH4FilpH) using platforms other than [Processing](https://processing.org/) and [P5Js](https://p5js.org/). For the 2D challenges, I am using [Rust](https://www.rust-lang.org/), and, for the 3D challenges, I am using the [Godot Game Engine](https://godotengine.org/).

This is mainly a pet project to practice creative coding and computer graphics.

## Progression

- Completed Challenges: 15

## Highlights

- \#2 Menger Sponge
![](002-menger-sponge/screenshot.png)

- \#9 Solar System Part 3
![](009-solar-system-3/screenshot.png)

- \#10 Maze Generator
![](010-maze-generator/screenshot.png)

- \#12 Lorenz Attractor
![](012-lorenz-attractor/screenshot.png)

- \#13 Reaction Diffusion Algorithm
![](013-reaction-diffusion/capture.gif)