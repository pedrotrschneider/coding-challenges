# Menger Sponge

[Youtube video](https://www.youtube.com/watch?v=LG8ZK-rRkXo)

Remake in Rust of Daniel Shiffman's Coding Challenge #2 Menger Sponge

![](screenshot.png)

You can run this coding challenge by importing the `project.godot` file in the Godot Game Engine