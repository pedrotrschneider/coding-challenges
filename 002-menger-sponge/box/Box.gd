# Box.gd
# Copyright (C) 2022  Pedro Tonini Rosenberg Schneider
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https:www.gnu.org/licenses/>.

class_name Box
extends Spatial

var BOX_SCENE: PackedScene = load("res://box/Box.tscn");

onready var mesh_instance: MeshInstance = $MeshInstance;
onready var children: Spatial = $Children;

var size: float = 2.0;
var divided: bool = false;


func _ready() -> void:
	var cube_mesh: CubeMesh = CubeMesh.new();
	cube_mesh.size = Vector3(size, size, size);
	mesh_instance.mesh = cube_mesh;


func generate() -> void:
	if not divided:
		for i in range(-1, 2):
			for j in range(-1, 2):
				for k in range(-1, 2):
					# warning-ignore:narrowing_conversion
					var sum: int = abs(i) + abs(j) + abs(k);
					if sum > 1:
						var new_box = BOX_SCENE.instance();
						var new_size: float = size / 3;
						new_box.size = new_size;
						children.add_child(new_box);
						new_box.transform.origin = Vector3(i * new_size, j * new_size, k * new_size);
		divided = true;
		self.mesh_instance.queue_free();
	else:
		for c in children.get_children():
			c.generate();
