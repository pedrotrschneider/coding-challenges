# World.gd
# Copyright (C) 2022  Pedro Tonini Rosenberg Schneider
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https:www.gnu.org/licenses/>.

extends Spatial

onready var sponge = $Sponge; 
onready var camera  =$Origin/Camera;


func _ready() -> void:
	camera.look_at(sponge.transform.origin, Vector3.UP);


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			sponge.generate();
