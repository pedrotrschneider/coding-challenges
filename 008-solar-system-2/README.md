# Solar System Part 2

[Youtube video](https://www.youtube.com/watch?v=dncudkelNxw)

Remake in Rust of Daniel Shiffman's Coding Challenge #8 Solar System Part 2

![](screenshot.png)

You can run this coding challenge by importing the `project.godot` file in the Godot Game Engine